package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name= "report")
public class Report {

	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;


	@NotBlank
	@Column
	private String content;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	// 永続化前に作成日時、更新日時をセット
	@PrePersist
	public void onPrePresist() {
		setCreatedAt(new Date());
		setUpdatedAt(new Date());
	}

	// 更新前に更新日時をセット
	@PreUpdate
	public void onPreUpdate() {
		setUpdatedAt(new Date());
	}


}
