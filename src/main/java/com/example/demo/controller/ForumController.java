package com.example.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// トップページ
	@GetMapping
	public ModelAndView top(@RequestParam("startDate") Optional<String> startDate, @RequestParam("endDate") Optional<String> endDate) {
		ModelAndView mav = new ModelAndView();

		// 開始日時が指定されていればstartに格納
		String start = null;
		if (startDate.isPresent()) {
			start = startDate.get();
			mav.addObject("start", startDate.get());
		}

		// 終了日時が指定されていればendに格納
		String end = null;
		if (endDate.isPresent()) {
			end = endDate.get();
			mav.addObject("end", endDate.get());
		}

		// 投稿用リスト
		List<Report> contentData;

		if (!StringUtils.hasText(start) && !StringUtils.hasText(end) ) {
			// 投稿を全件取得
			contentData = reportService.findAllReport();
		} else {
			// 絞り込みをした投稿を取得
			contentData = reportService.findByCreatedAtBetween(start, end);
		}

		// コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();

		// コメントform用の空のentityを生成し、保管
		Comment comment = new Comment();
		mav.addObject("commentForm", comment);

		// 画面遷移先を指定
		mav.setViewName("/top");

		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);

		// コメントデータを保管
		mav.addObject("comments", commentData);

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		// レポートform用の空のentityを生成し、保管
		Report report = new Report();
		mav.addObject("formModel", report);

		// 画面遷移先を指定
		mav.setViewName("/new");

		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {

		// 新規投稿をテーブルに格納
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		// 編集する投稿を取得
		Report report = reportService.findReport(id);

		// 編集する投稿を保管
		mav.addObject("formModel", report);

		// 画面遷移先を指定
		mav.setViewName("/edit");

		return mav;
	}

	// 更新処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

		// Idをセット
		report.setId(id);

		// 投稿の作成日時を取得し、編集した投稿にセット
		Date createdAt = reportService.findReport(id).getCreatedAt();
		report.setCreatedAt(createdAt);

		// 投稿を更新
		reportService.saveReport(report);

		return new ModelAndView("redirect:/");
	}
}
