package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{

	// 更新日時の降順でコメント全件取得
	public List<Comment> findAllByOrderByUpdatedAtDesc();

}
