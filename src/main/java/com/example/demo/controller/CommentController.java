package com.example.demo.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	@Autowired
	ReportService reportService;

	// コメント作成処理
	@PostMapping("/comment/{id}")
	public ModelAndView addComment(@PathVariable Integer id, @ModelAttribute("commentForm") Comment comment) {

		// 紐づく投稿のIdをセットし、テーブルに追加
		comment.setReportId(id);
		commentService.saveComment(comment);

		// 紐づく投稿を取得し、更新する
		Report report = reportService.findReport(id);
		report.setUpdatedAt(new Date());
		reportService.saveReport(report);

		return new ModelAndView("redirect:/");
	}

	// コメント編集画面
	@GetMapping("/comment/edit/{id}")
	public ModelAndView eidtComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		// コメントを取得し、保管
		Comment comment = commentService.findComment(id);
		mav.addObject("commentForm", comment);

		mav.setViewName("/commentEdit");

		return mav;
	}

	// コメント更新処理
	@PutMapping("/comment/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("commentForm") Comment comment) {

		comment.setId(id);
		// コメントの作成日時を取得し、セット
		Date createdAt = commentService.findComment(id).getCreatedAt();
		comment.setCreatedAt(createdAt);
		commentService.saveComment(comment);

		// 紐づく投稿を取得し、更新する
		int reportId = commentService.findComment(id).getReportId();
		Report report = reportService.findReport(reportId);
		report.setUpdatedAt(new Date());
		reportService.saveReport(report);

		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@DeleteMapping("/comment/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}
}
