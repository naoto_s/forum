package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// 更新日時の降順で全件取得
	public List<Report> findAllReport(){
		return reportRepository.findAllByOrderByUpdatedAtDesc();
	}

	// 指定された作成日時の範囲内の投稿を更新日時の降順で取得
	public List<Report> findByCreatedAtBetween(String start, String end){
		Timestamp startDate = null;
		Timestamp endDate = null;

		if (!StringUtils.hasText(start)) {
			startDate = Timestamp.valueOf("2021-01-01 00:00:00");		// 日付指定なければデフォルトの日時を格納
		} else {
			startDate = Timestamp.valueOf(start + " 00:00:00");
		}

		if (!StringUtils.hasText(end)) {
			endDate = new Timestamp(System.currentTimeMillis());		// 日付指定なければ現在の日時を格納
		} else {
			endDate = Timestamp.valueOf(end + " 23:59:59");
		}
		return reportRepository.findByCreatedAtBetweenOrderByUpdatedAtDesc(startDate, endDate);
	}

	// レコード追加・更新
	public void saveReport(Report report) {
		reportRepository.saveAndFlush(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード一件取得
	public Report findReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}
}
