package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer>{

	// 更新日時の降順で全件取得
	public List<Report> findAllByOrderByUpdatedAtDesc();

	// 指定された作成日時の範囲内の投稿を更新日時の降順で取得
	public List<Report> findByCreatedAtBetweenOrderByUpdatedAtDesc(Timestamp start, Timestamp end);
}
